﻿using System.Windows;

namespace SearchAllakhazam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            base.DataContext = new ViewModel();
        }
    }
}
