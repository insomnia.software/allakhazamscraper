﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;
using AllakhazamScraper;

namespace SearchAllakhazam
{
    class ViewModel
    {
        private string search;
        public string Search { get { return search; } set { search = value; OnPropertyChanged("Search"); } }

        private bool inPosts;
        public bool InPosts { get { return inPosts; } set { inPosts = value; OnPropertyChanged("InPosts"); } }

        private bool inTopics;
        public bool InTopics { get { return inTopics; } set { inTopics = value; OnPropertyChanged("InTopics"); } }
        
        private bool previousExpansions;
        public bool PreviousExpansions { get { return previousExpansions; } set { previousExpansions = value; OnPropertyChanged("PreviousExpansions"); } }
        
        private ObservableCollection<string> expansions = new ObservableCollection<string>{ "Classic", "Burning Crusade", "Wrath of the Lich King", "Cataclysm", "Pandaran" };
        public ObservableCollection<string> Expansions { get { return expansions; } set { expansions = value; OnPropertyChanged("Expansions"); } }

        private string selectedExpansion = "Classic";
        public string SelectedExpansion { get { return selectedExpansion; } set { selectedExpansion = value; OnPropertyChanged("SelectedExpansion"); } }

        private ObservableCollection<Topic> topics = new ObservableCollection<Topic>();
        public ObservableCollection<Topic> Topics { get { return topics; } set { topics = value; OnPropertyChanged("Topics"); } }
        
        private Topic selectedTopic;

        public Topic SelectedTopic
        {
            get
            {
                return selectedTopic;
            }
            set
            {
                selectedTopic = value;
                OnPropertyChanged("SelectedTopic");


            }
        }
        
        private Post selectedPost;
        public Post SelectedPost { get { return selectedPost; } set { selectedPost = value; OnPropertyChanged("SelectedPost"); } }
        

        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand ?? (searchCommand = new RelayCommand(param => DoSearch())); }
        }

        private void DoSearch()
        {
            SQLiteDatabase db = new SQLiteDatabase();
            db.OpenConnection();
            
            topics.Clear();
            db.GetTopics(Search, inPosts, inTopics).ForEach(x => Topics.Add(x));
            db.CloseConnection();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }

    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields

        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough]
        public bool CanExecute(object parameters)
        {
            return _canExecute == null ? true : _canExecute(parameters);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameters)
        {
            _execute(parameters);
        }

        #endregion // ICommand Members
    }
}
