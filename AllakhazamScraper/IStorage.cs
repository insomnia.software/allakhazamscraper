﻿using System.Collections.Generic;

namespace AllakhazamScraper
{
    public interface IStorage
    {
        bool AddTopic(string Id, string Name, string Author, string Expansion, string LastPost);
        bool AddPost(string TopicId, string Author, string Post);
        void CloseConnection();
        void OpenConnection();

        List<Topic> GetTopics(string searchText, bool inPosts = true, bool inTopics = true);
    }
}
