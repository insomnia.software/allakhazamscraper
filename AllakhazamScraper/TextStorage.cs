﻿using System.Collections.Generic;
using System.IO;

namespace AllakhazamScraper
{
    public class TextStorage : IStorage
    {
        private readonly TextWriter tw = new StreamWriter("output.txt", false);

        public bool AddTopic(string Id, string Name, string Author, string Expansion, string LastPost)
        {
            try
            {
                tw.WriteLine(Name + "\n" + Author);
            }
            catch (System.Exception)
            {
                return false;
            }
            return true;
        }

        public bool AddPost(string TopicId, string Author, string Post)
        {
            try
            {
                tw.WriteLine("\n" + Author + "\n" + Post);
            }
            catch (System.Exception)
            {
                return false;
            }
            return true;
        }

        public void CloseConnection()
        {
            tw.Close();
        }

        public void OpenConnection()
        {
            throw new System.NotImplementedException();
        }

        public List<Topic> GetTopics(string searchText, bool inPosts = true, bool inTopics = true)
        {
            throw new System.NotImplementedException();
        }
    }
}
