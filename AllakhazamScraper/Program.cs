﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace AllakhazamScraper
{
    public class Program
    {
        public static bool preBC = true;
        public static readonly IStorage db = new SQLiteDatabase();

        static void Main()
        {
            Console.WriteLine("Allakhazam wow forum scraper\n\nPress Enter to start");
            Console.ReadLine();

            string html = GetHtml("http://wow.allakhazam.com/forum.html?forum=21");

            int pages;
            if (!int.TryParse(Regex.Match(html, "<a href=\"/forum.html\\?forum=21&amp;p=([0-9][0-9]+)\"").Groups[1].ToString(), out pages)) //try and get the page count for the foum
                Console.WriteLine("Error"); //page count returned was not a number, regex failed


            Regex r = new Regex("<tr class=\"[dl]r\">(?:.|\\n)+?</tr>"); //regex for matching a topic
            
            List<Topic> topics = new List<Topic>();

            for (int i = pages; i > pages - 1; i--)// -1 is here as a debug limit
            {
                html = GetHtml("http://wow.allakhazam.com/forum.html?forum=21&p=" + i);
                topics.AddRange(from Match m in r.Matches(html) select new Topic(m.ToString())); //get all topics

                if (!preBC) //stop collecting once the release of BC has passed
                    break;
            }

            foreach (Topic topic in topics)
                topic.GetPosts();

            db.OpenConnection();


            foreach(Topic topic in topics)
            {
                if (!db.AddTopic(topic.Id, topic.Name, topic.Author, topic.Expansion.ToString(), topic.LastPost.ToShortDateString()))
                    Console.WriteLine("Critical DB error");

                foreach (Post post in topic.Posts)
                {
                    if (!db.AddPost(topic.Id, post.Author, post.Text))
                        Console.WriteLine("Critical DB error");
                }
            }
            db.CloseConnection();

            Console.WriteLine("Finished");
            Console.ReadKey();
        }

        public static string GetHtml(string url)
        {
            Console.WriteLine("Getting url: " + url);
            using (WebClient client = new WebClient())
            {
                try
                {
                    return client.DownloadString(url);
                }
                catch (WebException) //catch 404
                {
                    return "";
                }
            }
        }
    }

    public class Topic
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public string Id { get; set; }
        public int Pages { get; set; }
        public DateTime LastPost { get; set; }
        public List<Post> Posts { get; set; }
        public expansion Expansion { get; set; }

        public enum expansion
        {
            classic,
            burningCrusade,
            wrathOfTheLichKing,
            cataclysm,
            mistsOfPanderia
        };

        public Topic(string html)
        {
            Id = Regex.Match(html, "mid=([0-9]+)").Groups[1].ToString();
            Name = Regex.Match(html, " class=\"mba\">(.+)</a>").Groups[1].ToString();
            Author = Regex.Match(html, "<td class=\"startedby\"><a href=\"/users/(.+)?\" class=").Groups[1].ToString();
            LastPost = DateTime.Parse(Regex.Match(html, "[ap]m\">(.+?)</a>").Groups[1].ToString());

            if (LastPost <= new DateTime(2007, 01, 17))
                Expansion = expansion.classic;
            else if (LastPost <= new DateTime(2008, 11, 14))
            {
                Expansion = expansion.burningCrusade;
                Program.preBC = false;
            }
            else if (LastPost <= new DateTime(2010, 12, 7))
                Expansion = expansion.wrathOfTheLichKing;
            else if (LastPost <= new DateTime(2010, 9, 25))
                Expansion = expansion.cataclysm;
            else
                Expansion = expansion.mistsOfPanderia;

            string pages = Regex.Match(html, ">([0-9]+)</a> <a href=\"/forum.html\\?forum=21&amp;mid=[0-9]+&amp;p=[0-9]\" class=\"non-box next\">Next &raquo;</a><div class=\"clear\"></div></div>").Groups[0].ToString();
            Pages = string.IsNullOrEmpty(pages) ? 1 : int.Parse(pages);

        }

        public Topic(){}

        public bool GetPosts()
        {
            Posts = new List<Post>();
            //string html = Program.GetHtml("http://wow.allakhazam.com/forum.html?forum=21&mid=" + Id);
            Regex r = new Regex("<a class=\"button post-link\"(?:.|\n)+?Page top</a></div>");

            for (int i = 0; i < Pages; i++)
            {
                Posts.AddRange(from Match m in r.Matches(Program.GetHtml("http://wow.allakhazam.com/forum.html?forum=21&mid=" + Id)) select new Post(m.ToString()));
                //html = Program.GetHtml("http://wow.allakhazam.com/forum.html?forum=21&mid=" + Id + "&p=" + i);
            }

            return true;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class Post
    {
        public string Author { get; set; }
        public string Text { get; set; }
        public int PostNumber;

        public Post(){}

        public Post(string html)
        {
            int.TryParse(Regex.Match(html,"href=\"#([0-9]+)\"").Groups[1].ToString(), out PostNumber);
            Author = Regex.Match(html, "<div class=\"user [A-Za-z]*\"><div class=\"name\"><a href=\"/users/([A-Za-z0-9]+)").Groups[1].ToString();
            Text = Regex.Match(html, "<div class=\"post-text\">((:?\n|.)+?)</div>\\s+<div class=\"f-toplink\">").Groups[1].ToString();
        }

        public override string ToString()
        {
            Text = Text.Replace("\n", "");
            Text = Text.Trim(new[] {' '});

            if (Text.Length > 75)
                return Text.Substring(0, 75) + "....";
            
            return Text + "....";
        }
    }
}
