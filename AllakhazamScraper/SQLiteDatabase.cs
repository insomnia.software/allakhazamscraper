﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace AllakhazamScraper
{
    public class SQLiteDatabase : IStorage
    {
        //static readonly string location = System.IO.Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location);
        private readonly SQLiteConnection mySQLConnection = new SQLiteConnection("Data Source=C:\\Allakhazam.s3db");

        public bool AddTopic(string Id, string Name, string Author, string Expansion, string LastPost)
        {
            //try
            //{
                Name = Name.Replace("'", "''");//keep SQL happy with apostrophe's
                SQLiteCommand myCommand = new SQLiteCommand("INSERT INTO Topics (Id, Name, Author, Expansion, LastPost) VALUES ('" + Id + "', '" + Name + "', '" + Author + "', '" + Expansion + "', '" + LastPost + "');", mySQLConnection);
                myCommand.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //    return false;
            //}
            return true;
        }

        public bool AddPost(string TopicId, string Author, string Text)
        {
            //try
            //{
                Text = Text.Replace("'", "''"); //keep SQL happy with apostrophe's
                SQLiteCommand myCommand = new SQLiteCommand("INSERT INTO Posts (TopicId, Author, Post) VALUES ('" + TopicId + "', '" + Author + "', '" + Text + "');", mySQLConnection);
                myCommand.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //    return false;
            //}
            return true;
        }

        public void CloseConnection()
        {
            mySQLConnection.Close();
        }

        public void OpenConnection()
        {
            mySQLConnection.Open();
        }

        public List<Topic> GetTopics(string searchText, bool inPosts = true, bool inTopics = true)
        {
            SQLiteCommand myCommand;
            if(string.IsNullOrEmpty(searchText) || string.IsNullOrWhiteSpace(searchText))
                myCommand = new SQLiteCommand("SELECT Id, Name, Author, Expansion, LastPost FROM Topics", mySQLConnection);
            else
            {
                if(!inPosts && inTopics)//topics only
                    myCommand = new SQLiteCommand("SELECT Id, Name, Author, Expansion, LastPost FROM Topics WHERE Name LIKE '%" + searchText + "%'", mySQLConnection);
                else if (inPosts && !inTopics) //posts only
                    myCommand =
                        new SQLiteCommand("SELECT Id, Topics.Name, Topics.Author, Expansion, LastPost, TopicId, Posts.Author, Post FROM Topics INNER JOIN Posts ON Topics.Id==Posts.TopicId WHERE Post LIKE '%" + searchText + "%' COLLATE NOCASE", mySQLConnection);
                else //fix this
                    throw new NotImplementedException();
            }

            SQLiteDataReader myReader = myCommand.ExecuteReader();

            List<Topic> topics = new List<Topic>();

            if (!myReader.HasRows) return topics;

            while (myReader.Read()) //for each row
            {
                if (!inPosts && inTopics)
                    topics.Add(new Topic { Id = myReader.GetString(0), Name = myReader.GetString(1), Author = myReader.GetString(2), Expansion = (Topic.expansion)Enum.Parse(typeof(Topic.expansion), myReader.GetString(3), true), LastPost = DateTime.Parse(myReader.GetString(4)) });
                else if (inPosts && !inTopics) //posts only
                {
                    string Id = myReader.GetString(0);
                    if (topics.All(topic => topic.Id != Id))//doesn't contain the topic, so add the topic and the post found
                    {
                        topics.Add(new Topic { Id = myReader.GetString(0), Name = myReader.GetString(1), Author = myReader.GetString(2), Expansion = (Topic.expansion)Enum.Parse(typeof(Topic.expansion), myReader.GetString(3), true), LastPost = DateTime.Parse(myReader.GetString(4)), Posts = new List<Post>{ new Post{Text = myReader.GetString(7)}}});                            
                    }
                    else//topic already present in the list, 
                    {
                        //topics.Where(x => x.Id == Id)
                        //    .ToList()
                        //    .ForEach(x => x.Posts.Add(new Post {Text = myReader.GetString(7)}));

                        topics.First(x => x.Id == Id).Posts.Add(new Post { Text = myReader.GetString(7) }); //find topic with id we want and add the new post found to the lists of posts in the topic object

                        //foreach (Topic topic in topics)
                        //{
                        //    if(topic.Id == Id)
                        //        topic.Posts.Add(new Post { Text = myReader.GetString(7) });
                        //}
                    }
                }
                else if (inPosts && inTopics)
                {
                    
                }
            }
            myReader.Close();

            return topics;
        }
    }
}
